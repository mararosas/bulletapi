﻿/*This file is part of BulletAPI
    2014 Max J. Rodríguez Beltran ing.maxjrb[at]gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BulletAPI
{
    public class BulletAPI
    {
        private static readonly Uri BaseUri = new Uri("https://api.pushbullet.com");

        /// <summary>
        /// Method that returns a list of devices available
        /// </summary>
        /// <param name="bCert">If this param is True will accept all certifications (kind of insecure) but useful to run it on Mono</param>
        /// <param name="apiKey">The API Key that you get in your Pushbullet account</param>
        /// <returns><c>Dictionary(device_iden,model or nickname)</c></returns>
        public static Dictionary<string, string> GetDevices(bool bCert, string apiKey)
        {
            var devList = new Dictionary<string, string>();

            using (var wClient = GetClient(apiKey,bCert))
            {
                //Definimos que el contenido de la respuesta será en formato JSON
                //Set to JSON the response type 
                wClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                try
                {
                    //Llamamos de forma asincrona la url base + el prefijo del servicio para los dispositivos (https://api.pushbullet.com/api/devices)
                    //Asynchronous call to the API method to get device list
                    Task<string> getStringTask = wClient.GetStringAsync(new Uri(BaseUri, "/api/devices"));

                    //Guardarmos en un string el resultado en formato JSON
                    //We keep the JSON response on string
                    var result = getStringTask.Result;

                    //Creamos un nuevo objeto de tipo JSON (JObject) indicandole que deserialice el resultado que guardamos anteriormente
                    //Create a JSON object (JObject) to deserialize the result 
                    var devices = new JObject((JObject)JsonConvert.DeserializeObject(result));

                    //Creamos un array de tipo JSON donde estarán los dispositivos
                    //Create an JSON Array(JArray) where we'll keep the device list
                    var devs = (JArray)devices["devices"];

                    //Recorremos los dispositivos que estén en el JArray
                    //Then we loop through the device list
                    foreach (var dev in devs)
                    {
                        //Agregamos el elemento "iden" del dispositivo
                        //Add the "iden" element from the device
                        //Y validamos, si el elemento "extras"->"Nickname" esta vacío usamos el elemento "model" 
                        //And validate, if the "extras"->"Nickname" element is empty then we use "model" element.
                        devList.Add(dev.SelectToken("iden").ToString(),
                            dev["extras"]["nickname"] != null
                                ? dev["extras"]["nickname"].ToString()
                                : dev["extras"]["model"].ToString());
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return devList;
        }

        /// <summary>
        /// Method to push (Notes, Links, Address, and Lists)
        /// </summary>
        /// <param name="bCert">If this param is True will accept all certifications (kind of insecure) but useful to run it on Mono</param>
        /// <param name="apiKey">The API Key that you get in your Pushbullet account</param>
        /// <param name="type">Type of push, can be: <c>note,link,address,list</c></param>
        /// <param name="iddev">Device id (to which device push it)</param>
        /// <param name="message">Body of the push (Note message|Link|Address|List items</param>
        /// <param name="title">Optional parameter, Title of the push</param>
        public static void PushIt(bool bCert, string apiKey, string type, string iddev, string message, string title = "" )
        {
            using (var wc = GetClient(apiKey,bCert))
            {
                var parameters = String.Format("device_iden={0}&type={1}", iddev, type);

                switch (type)
                {
                    case "note":
                        parameters += String.Format("&title={0}&body={1}", title, message);
                        break;
                    case "link":
                        parameters += String.Format("&title={0}&url={1}", title, message);
                        break;
                    case "address":
                        parameters += String.Format("&name={0}&address={1}", title, message);
                        break;
                    case "list":
                        parameters += String.Format("&title={0}", title);
                        parameters += message;
                        break;
                    default:
                        throw new UndefinedTypeException("Type not defined, the types are: note,link,address,list");
                }
                //Set request headers to accept JSON
                wc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                /*Llamamos de forma asincrona la url base + el prefijo del servicio para hacer enviar el contenido (https://api.pushbullet.com/api/pushes)
                 *Asynchronous call to the API method to send the content
                 */
                var resp = wc.PostAsync(new Uri(BaseUri, "/api/pushes"), new StringContent(parameters, Encoding.UTF8, "application/x-www-form-urlencoded"));
                Task<string> result = resp.Result.Content.ReadAsStringAsync();
                //string resultado = result.Result; //we can read the response
            }
        }

        /// <summary>
        /// Method to push files
        /// </summary>
        /// <param name="bCert">If this param is True will accept all certifications (kind of insecure) but useful to run it on Mono</param>
        /// <param name="apiKey">The API Key that you get in your Pushbullet account</param>
        /// <param name="pathFile">Path to file to upload</param>
        /// <param name="iddev">Device id (to which device push it)</param>
        public static void PushFile(bool bCert, string apiKey, string pathFile, string iddev)
        {
            string name = Path.GetFileName(pathFile);
            using (var wc = GetClient(apiKey,bCert))
            {
                //Create a multipartformdata content
                using (var multiPartCont = new MultipartFormDataContent())
                {
                    //As we can see in the API Doc

                    //curl -i https://api.pushbullet.com/api/pushes \
                    //-u API_KEY:
                    //-F device_iden=u1qSJddxeKwOGuGW
                    //-F type=file
                    //-F file=@test.txt

                    //We need to add in the multipartcontent the deviceid and type that we are pushing
                    //First Add the device id 
                    multiPartCont.Add(AddContent("device_iden", iddev));
                    //Then add the type "file"
                    multiPartCont.Add(AddContent("type", "file"));
                    //Finally we add the file on a stream
                    multiPartCont.Add(AddContent(new FileStream(pathFile, FileMode.Open), name));

                    try
                    {
                        //Asynchronous call to the API method to send the content
                        var resp = wc.PostAsync(new Uri(BaseUri, "api/pushes"), multiPartCont);
                        Task<string> result = resp.Result.Content.ReadAsStringAsync();
                        string resultado = result.Result;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Method to add file to Stream Content
        /// </summary>
        /// <param name="stream">Stream of file</param>
        /// <param name="filename">The name of the file</param>
        /// <returns>Streamcontent to add to Multipartformdata</returns>
        private static StreamContent AddContent(Stream stream, string filename)
        {
            //Create a new StreamContent that gets the stream from params
            var fileContent = new StreamContent(stream);
            //Add the header Disposition Content 
            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = "\"file\"",
                FileName = "\"" + filename + "\""
            };
            //Set the content Type header to "application/octet-stream"
            fileContent.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            return fileContent;
        }
        //method to add params needed by API (device_iden and type)
        private static StringContent AddContent(string name, string content)
        {
            //Create String content setting the value in this case would be the "device_iden" or "type"
            var fileContent = new StringContent(content);
            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = "\"" + name + "\""
            };
            return fileContent;
        }

        /// <summary>
        /// Method to create a http client with authentication to call the API Service
        /// </summary>
        /// <param name="apiKey">The API Key that you get in your Pushbullet account</param>
        /// <param name="bCert">If this param is True will accept all certifications (kind of insecure) but useful to run it on Mono</param>
        /// <returns>Httpclient with authenticacion headers</returns>
        private static HttpClient GetClient(string apiKey, bool bCert)
        {
            if (bCert)
            //On Mono this prevents Certification error, or you can Import certificates to your pc with "certmgr -ssl https://pushbullet.com" and disable this line.
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, errors) => { return true; };

            var wClient = new HttpClient();

            if (apiKey != "")
            {
                // Para las cabeceras de autenticación básica de HTTP convertimos a base 64 la llave de la API que nos proporcionaron
                apiKey = Convert.ToBase64String(Encoding.UTF8.GetBytes(apiKey + ":"));
            }
            else
            {
                throw new ApiKeyNotFoundException("ApiKey not defined... please set one");
            }
            //Definimos las cabeceras de autenticación
            wClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", apiKey);
            var timeOut = new TimeSpan(0, 0, 10, 0);
            wClient.Timeout = timeOut;

            return wClient;
        }


    }
    [Serializable]
    public class _ErrorException : Exception
    {
        public string ErrorMessage
        {
            get
            {
                return base.Message.ToString();
            }
        }

        public _ErrorException(string errorMessage)
            : base(errorMessage) { }

        public _ErrorException(string errorMessage, Exception innerEx)
            : base(errorMessage, innerEx) { }
    }

    public class ApiKeyNotFoundException : _ErrorException
    {
        public ApiKeyNotFoundException(string message)
            : base(message)
        {
        }

        public ApiKeyNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    public class UndefinedTypeException : _ErrorException
    {
        public UndefinedTypeException(string message)
            : base(message)
        {
        }

        public UndefinedTypeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
